const fs = require('fs');
const csv = require('fast-csv');
const { Alchemy, Network } = require("alchemy-sdk");
const cheerio = require('cheerio');
const axios = require('axios');
const { runMainn } = require('./total-token');


// Wallet address 0xd8da6bf26964af9d7eed9e03e53415d37aa96045
// const address = "0x8960486E075a47aA9E2E500a369310550af45B9A";

const config = {
  apiKey: "mgVo5d5t7BrzlHeOHNEiX9_lD4dD4Tbv",
  network: Network.ETH_MAINNET,
};
const alchemy = new Alchemy(config);

async function scrapePrice(adddresss,idd) {
  const response = await axios.get(`https://etherscan.io/nft/${adddresss}/${idd}`);
//   console.log(response.data);
  const a = cheerio.load(response.data);
  const pprice=a('.mr-1.text-truncate.font-size-md').text();
  // const pprice = a('.text-danger strong').text();
  return pprice;
}


const now = new Date();

let nft_info=[];

const main= async(address)=>{
   
    // Get all NFTs
  const nfts = await alchemy.nft.getNftsForOwner(address);
        // console.log(nfts);
         // Parse output
  const numNfts = nfts["totalCount"];
  const nftList = nfts["ownedNfts"];
      console.log(`Total NFTs owned by ${address}: ${numNfts} \n`);
      for (let nft of nftList) {
        let pprice=await scrapePrice(nft.contract.address,nft.tokenId);
        console.log(pprice);
        nft_info.push({
          "tokenId":`${nft.tokenId}`,
          "Owner_Address":`${address}`,
          "Date_Time":`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()} -- ${now.getHours()}:${now.getMinutes()}`,
          "Name":`NFT::${nft.title}`,
          "Token":`${nft.tokenType}`,
          "Contract Address":`${nft.contract.address}`,
          "Description":`${nft.description}`,
          "Price":pprice,
          "Symbol":"Nft",
          "Balance":"1",
        });
      }
   
}

const runMain = async (address) => {
  try {
    await main(address);
    console.log(nft_info);
    // Read the existing data from the CSV file
let existingData = [];
fs.createReadStream('nft_info.csv')
  .pipe(csv.parse({ headers: true }))
  .on('data', (row) => {
    // add the existing data to an array
    existingData.push(row);
  })
  .on('end', () => {
    // combine the existing data and the new JSON data
    const jsonData = [...existingData, ...nft_info];
    // jsonData.sort((a,b)=>{
    //   if(a.NFT_Name.toUpperCase()<b.NFT_Name.toUpperCase()) return -1;
    //   if(a.NFT_Name.toUpperCase()>b.NFT_Name.toUpperCase()) return 1;
    //   if(a.NFT_Name.toUpperCase()===b.NFT_Name.toUpperCase()){
    //     if(a.Date_Time.toUpperCase()>b.Date_Time.toUpperCase()) return 1;
    //     else return -1;
    //   }
    // });

    // write the combined data back to the CSV file
    const ws = fs.createWriteStream("nft_info.csv");
    csv
     .write(jsonData, {headers: true})
     .pipe(ws);
  });
    // process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

const addresses=["0x8960486E075a47aA9E2E500a369310550af45B9A"];
for (let address of addresses){
  runMain(address);
  runMainn(address);
}
