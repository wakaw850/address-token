// Setup: npm install alchemy-sdk
const { Alchemy, Network } = require("alchemy-sdk");
const fs = require('fs');
const csv = require('fast-csv');
const ethers =require('ethers');const axios = require('axios');





const CMC_API_KEY = 'd45de7c5-ce21-4843-9eff-5a0130455e82';

async function getTokenPrice(symbol) {
    const url = `https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=${symbol}`;

    const headers = {
        'X-CMC_PRO_API_KEY': CMC_API_KEY,
    };

    try {
        const response = await axios.get(url, { headers });
        const data = response.data;
        const tokenData = data.data[symbol];
        return tokenData.quote.USD.price;
    } catch (error) {
        // console.log(error);
    }
}

const config = {
  apiKey: "mgVo5d5t7BrzlHeOHNEiX9_lD4dD4Tbv",
  network: Network.ETH_MAINNET,
};

// Wallet address 0xd8da6bf26964af9d7eed9e03e53415d37aa96045
// const address = "0xFA4992db9dbe69F5C29a1697054750757D486FE9";

const provider = new ethers.providers.WebSocketProvider('https://eth-mainnet.g.alchemy.com/v2/mgVo5d5t7BrzlHeOHNEiX9_lD4dD4Tbv');

async function checkBalance(address) {
    balance = await provider.getBalance(address);
    return (ethers.utils.formatEther(balance));
}


const now = new Date();

let token_info=[];



const alchemy = new Alchemy(config);

const main = async (address) => {

  // Get token balances
  const balances = await alchemy.core.getTokenBalances(address);

  // Remove tokens with zero balance
  const nonZeroBalances = balances.tokenBalances;/*.tokenBalances.filter((token) => {
    return token.tokenBalance !== "0";
  });*/

  console.log(`Token balances of ${address} \n`);
  
  const Ethbal= await checkBalance(address);
  const EthPrice = await getTokenPrice('ETH');


  token_info.push({
    "tokenId":"Token",
    "Owner_Address":address,
    "Date_Time":`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()} -- ${now.getHours()}:${now.getMinutes()}`,
    "Name":"Token",
    "Token":"Ethereum",
    "Contract Address":"0x",
    "Description":"this is Ethereum",
    "Price":`$${EthPrice}`,
    "Symbol":"ETH",
    "Balance":Ethbal
  });

  // Counter for SNo of final output
  // let i = 2;

  // Loop through all tokens with non-zero balance
  for (let token of nonZeroBalances) {
    // Get balance of token
    let balance = token.tokenBalance;

    // Get metadata of token
    const metadata = await alchemy.core.getTokenMetadata(token.contractAddress);

    // Compute token balance in human-readable format
    balance = balance / Math.pow(10, metadata.decimals);
    balance = balance.toFixed(2);

    const tokenPrice = await getTokenPrice(metadata.symbol);

    // Print name, balance, and symbol of token
    // console.log(`${i++}. ${metadata.name} address(${token.contractAddress}): ${balance} ${metadata.symbol}`);
    token_info.push({
      // "Number":i++,
    "tokenId":"Token",
    "Owner_Address":address,
    "Date_Time":`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()} -- ${now.getHours()}:${now.getMinutes()}`,
    "Name":"Token",
    "Token":metadata.name,
    "Contract Address":token.contractAddress,
    "Description":"this is token",
    "Price":`$${tokenPrice?tokenPrice:"Not Available"}`,
    "Symbol":metadata.symbol,
    "Balance":balance
    });
  }

};

module.exports.runMainn = async (address) => {
  try {
    await main(address);
    console.log(token_info);
    // Read the existing data from the CSV file
let existingData = [];
fs.createReadStream('nft_info.csv')
  .pipe(csv.parse({ headers: true }))
  .on('data', (row) => {
    // add the existing data to an array
    existingData.push(row);
  })
  .on('end', () => {
    // combine the existing data and the new JSON data
    const jsonData = [...existingData, ...token_info];
    // jsonData.sort((a,b)=>{
    //   if(a.Token.toUpperCase()<b.Token.toUpperCase()) return -1;
    //   if(a.Token.toUpperCase()>b.Token.toUpperCase()) return 1;
    //   if(a.Token.toUpperCase()===b.Token.toUpperCase()){
    //     if(a.Date_Time.toUpperCase()>b.Date_Time.toUpperCase()) return 1;
    //     else return -1;
    //   }
    // });

    // write the combined data back to the CSV file
    const ws = fs.createWriteStream("nft_info.csv");
    csv
     .write(jsonData, {headers: true})
     .pipe(ws);
  });
    // process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

// const addresses=["0xFA4992db9dbe69F5C29a1697054750757D486FE9"];
// for (let address of addresses){
//   runMainn(address);
// }

