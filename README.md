# Token Balance Checker

This script uses the [Alchemy SDK](https://docs.alchemyapi.io/) to interact with the Ethereum blockchain and retrieve the token balances of a given wallet address.

## Setup
1. Install the required dependencies by running the following command:
```
npm install alchemy-sdk
npm install fast-csv
npm install ethers
```
2. Replace the placeholder API key in the `config` object with your own valid API key.

## Usage
1. Update the `address` variable in the `main` function with the wallet address you want to check the token balances for.
2. Run the script with the following command:
```
node analysis.js
```

The script will output the name, balance, and symbol of each token with a non-zero balance for the given wallet address. The output will be in the format of json and a csv file.

## For AutoSaving Data Daily
On Windows, you can use the Task Scheduler to schedule a JavaScript program to run at a specific time every day. You can do this by following these steps:

-Press Windows+R to open the Run dialog box.

-Type taskschd.msc and press Enter to open the Task Scheduler.

-Click on the "Action" menu and select "Create Basic Task".

-Give the task a name and description, and then click Next.

-Select "Daily" as the trigger and set the time when you want the task to run.

-Select "Start a program" as the action.

-In the Program/script box, type the full path of your Node.js command, and in the Add arguments box, type the full path of your JavaScript program, for example:
```
node "C:\path\to\your\javascript\program.js"

```
