const { Alchemy, Network } = require("alchemy-sdk");

const config = {
    apiKey: "mgVo5d5t7BrzlHeOHNEiX9_lD4dD4Tbv",
    network: Network.ETH_MAINNET,
};
const alchemy = new Alchemy(config);


let address = "0x8960486E075a47aA9E2E500a369310550af45B9A";
let contract_address =["0x3b484b82567a09e2588a13d54d032153f0c0aee0","0x2baa77b867590863b9de0dd1c5ff0e14da2fa140"];
let contract_addresses=[];
  
const main= async(address)=>{
    //For Tokens
    const balances = await alchemy.core.getTokenBalances(address);
    const nonZeroBalances = balances.tokenBalances.filter((token) => {
        return token.tokenBalance !== "0";
    });
   
    // For NFTs
  const nfts = await alchemy.nft.getNftsForOwner(address);
  const nftList = nfts["ownedNfts"];


    for (let nft of nftList) {
        contract_addresses.push(nft.contract.address);
    }

    for (let token of nonZeroBalances) {
      contract_addresses.push(token.contractAddress)
    }
}

runMainn = async (address) => {
    try {
        await main(address);
        for (let aa of contract_addresses) {
            if(aa === contract_address[0] || aa === contract_address[1]){
                console.log("Approve");
                return;
            }
        }
        console.log("Disapprove");
    }
    catch (error) {
        console.log(error);
        process.exit(1);
      }
}

runMainn(address);
